# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [0.1.1]

### Added
 - Version info on store and config screen

## [0.1.0] - 2023-11-03

### Added

 - Basic todo list with effort based system
 - vuex-store with retrocompatibility with other non-tagged versions