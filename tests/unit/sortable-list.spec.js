import { shallowMount } from '@vue/test-utils'
import SortableList from '@/components/SortableList.vue'
import TodoList from '@/components/TodoList.vue'

describe('SortableList.vue', () => {
  const todos = [
    { taskName: '2nd task', tiring: 3, date: '2020-05-03' },
    { taskName: 'first task', tiring: 1, date: '2020-05-01' },
    { taskName: 'last task', tiring: 5 }
  ]
  it('renders message if no todos to show', () => {
    const wrapper = shallowMount(SortableList, {
      propsData: { todos: [] }
    })
    expect(wrapper.find('span').text()).toMatch('Nothing')
  })
  it('renders todolist', () => {
    const wrapper = shallowMount(SortableList, {
      propsData: {
        todos: [{ id: '1' }]
      }
    })
    expect(wrapper.find(TodoList).exists()).toBe(true)
  })
  it('button reorder todos by effort', async () => {
    const wrapper = shallowMount(SortableList, {
      propsData: { todos }
    })
    wrapper.find('button.by-effort').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find(TodoList).props('todos')[0].taskName)
      .toMatch('first task')
  })
  it('button reorder todos by default', async () => {
    const wrapper = shallowMount(SortableList, {
      propsData: { todos }
    })
    wrapper.find('button.by-effort').trigger('click')
    await wrapper.vm.$nextTick()
    wrapper.find('button.default').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find(TodoList).props('todos')[0].taskName)
      .toMatch('2nd task')
  })
  it('button reorder todos by effort asc', async () => {
    const wrapper = shallowMount(SortableList, {
      propsData: { todos }
    })
    wrapper.find('button.by-effort').trigger('click')
    await wrapper.vm.$nextTick()
    wrapper.find('button.by-effort').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find(TodoList).props('todos')[0].taskName)
      .toMatch('last task')
  })
  it('button reorder todos by date', async () => {
    const wrapper = shallowMount(SortableList, {
      propsData: { todos }
    })
    wrapper.find('button.by-date').trigger('click')
    await wrapper.vm.$nextTick()
    expect(wrapper.find(TodoList).props('todos')[0].taskName)
      .toMatch('first task')
  })
})
