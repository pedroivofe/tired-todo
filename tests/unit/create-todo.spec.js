import { shallowMount } from '@vue/test-utils'
import { Store } from 'vuex-mock-store'
import CreateTodo from '@/components/CreateTodo.vue'
import InputTodo from '@/components/todo/InputTodo.vue'
import { convert } from '@/services/dateSameDay.js'

const mockTodo = { taskName: 'task name' }
const store = new Store({
  state: {
    todo: {}
  }
})
const mocks = {
  $store: store
}

describe('CreateTodo.vue', () => {
  let wrapper
  afterEach(() => store.reset())
  beforeEach(() => {
    wrapper = shallowMount(CreateTodo, { mocks })
  })
  it('show input todo', () => {
    expect(wrapper.find(InputTodo).exists()).toBe(true)
  })
  it('creates a new todo when input emits the event', async () => {
    wrapper.find(InputTodo).vm.$emit('input', mockTodo)
    await wrapper.vm.$nextTick()
    expect(store.dispatch)
      .toHaveBeenCalledWith('todo/newTodo', expect.objectContaining({
        tiring: expect.any(Number),
        taskName: expect.any(String)
      }))
  })

  it('creates a new todo with given prop date', async () => {
    const mockDate = '2020-04-21'
    wrapper = shallowMount(CreateTodo, {
      propsData: {
        date: convert(mockDate + ' 00:00:00')
      },
      mocks
    })
    wrapper.find(InputTodo).vm.$emit('input', mockTodo)
    await wrapper.vm.$nextTick()
    expect(store.dispatch)
      .toHaveBeenCalledWith('todo/newTodo', expect.objectContaining({
        tiring: expect.any(Number),
        date: mockDate,
        taskName: expect.any(String)
      }))
  })
  it('resets input after creating new todo', async () => {
    const inputTodo = wrapper.find(InputTodo)
    inputTodo.vm.$emit('input', mockTodo)
    await wrapper.vm.$nextTick()
    expect(inputTodo.props('value')).toEqual(expect.objectContaining({
      tiring: 1,
      taskName: ''
    }))
  })
})
