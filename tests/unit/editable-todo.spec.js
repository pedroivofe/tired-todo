import { shallowMount } from '@vue/test-utils'
import { Store } from 'vuex-mock-store'
import EditableTodo from '@/components/EditableTodo.vue'
import InputTodo from '@/components/todo/InputTodo.vue'
import TiredTodo from '@/components/todo/TiredTodo.vue'

const mockTodo = { taskName: 'task name' }
const store = new Store({
  state: {
    todo: {}
  },
  getters: {
    'todo/getTodoById': () => mockTodo
  }
})
const mocks = {
  $store: store
}

describe('EditableTodo.vue', () => {
  let wrapper
  afterEach(() => store.reset())
  beforeEach(() => {
    wrapper = shallowMount(EditableTodo, { mocks })
  })
  it('shows component todo', () => {
    expect(wrapper.find(TiredTodo).exists()).toBe(true)
  })
  it('show input todo when focused', async () => {
    wrapper.trigger('focus')
    await wrapper.vm.$nextTick()
    expect(wrapper.find(InputTodo).exists()).toBe(true)
  })
  it('updates todo when input emits the event', async () => {
    wrapper.trigger('focus')
    await wrapper.vm.$nextTick()
    wrapper.find(InputTodo).vm.$emit('input', mockTodo)
    await wrapper.vm.$nextTick()
    expect(store.dispatch).toHaveBeenCalledWith('todo/updateTodo', mockTodo)
  })
  it('updates todo when focust is out', async () => {
    wrapper.trigger('focusout')
    await wrapper.vm.$nextTick()
    expect(store.dispatch).toHaveBeenCalledWith('todo/updateTodo', mockTodo)
  })
})
