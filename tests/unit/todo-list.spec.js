import { shallowMount } from '@vue/test-utils'
import TodoList from '@/components/TodoList.vue'
import EditableTodo from '@/components/EditableTodo.vue'

describe('TodoList.vue', () => {
  it('show editable todos', () => {
    const wrapper = shallowMount(TodoList, {
      propsData: {
        todos: [{ id: '1' }]
      }
    })
    expect(wrapper.find(EditableTodo).exists()).toBe(true)
  })
})
