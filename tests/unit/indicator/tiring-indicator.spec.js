import { shallowMount } from '@vue/test-utils'
import TiringIndicator from '@/components/indicator/TiringIndicator.vue'

describe('TiringIndicator.vue', () => {
  it('show 😓 when tiring = 1', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 1 }
    })
    expect(wrapper.find('span').text()).toMatch('😓')
  })
  it('show 😓😓😓 when tiring = 3', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 3 }
    })
    expect(wrapper.find('span').text()).toMatch('😓😓😓')
  })
  it('show 😫 when tiring = 5', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 5 }
    })
    expect(wrapper.find('span').text()).toMatch('😫')
  })
  it('show 😫😫 when tiring = 10', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 10 }
    })
    expect(wrapper.find('span').text()).toMatch('😫😫')
  })
  it('show 😫😫 😓😓 when tiring = 12', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 12 }
    })
    expect(wrapper.find('span').text()).toMatch('😫😫 😓😓')
  })
  it('show 💀 when tiring = 50', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 50 }
    })
    expect(wrapper.find('span').text()).toMatch('💀')
  })
  it('show 💀💀 😫 when tiring = 105', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 105 }
    })
    expect(wrapper.find('span').text()).toMatch('💀💀 😫')
  })
  it('show 💀💀 😫 😓😓 when tiring = 107', () => {
    const wrapper = shallowMount(TiringIndicator, {
      propsData: { tiring: 107 }
    })
    expect(wrapper.find('span').text()).toMatch('💀💀 😫 😓😓')
  })
})
