import { shallowMount } from '@vue/test-utils'
import TiredStatus from '@/components/indicator/TiredStatus.vue'

describe('TiredStatus.vue', () => {
  it("says it's okay if you've done nothing yet", () => {
    const wrapper = shallowMount(TiredStatus, {
      propsData: {
        period: 'period',
        tired: 0
      }
    })
    expect(wrapper.find('span').text()).toMatchMessage("This period you've done nothing so far and it's okay 😌")
  })
  it("says it's okay if you've not done \"enough\"", () => {
    const wrapper = shallowMount(TiredStatus, {
      propsData: {
        period: 'period',
        tired: 4,
        maxTiredAcceptable: 5
      }
    })
    expect(wrapper.find('span').text()).toMatchMessage("This period you've done .* and it's okay 😌")
  })
  it("says you should stop if you're tired", () => {
    const wrapper = shallowMount(TiredStatus, {
      propsData: {
        period: 'period',
        tired: 5,
        maxTiredAcceptable: 5
      }
    })
    expect(wrapper.find('span').text()).toMatchMessage("This period you've done .* and you should stop 🙄")
  })
})
