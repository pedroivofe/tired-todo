import { shallowMount } from '@vue/test-utils'
import { Store } from 'vuex-mock-store'
import WeekTiredStatus from '@/components/indicator/WeekTiredStatus.vue'
import TiredStatus from '@/components/indicator/TiredStatus.vue'

const maxTiredAcceptablePerWeek = 30
const tiredThisWeek = 20
const store = new Store({
  state: {
    todo: {},
    config: { maxTiredAcceptablePerWeek }
  },
  getters: {
    'todo/tiredThisWeek': tiredThisWeek
  }
})
const mocks = {
  $store: store
}

describe('WeekTiredStatus.vue', () => {
  let wrapper
  afterEach(() => store.reset())
  beforeEach(() => {
    wrapper = shallowMount(WeekTiredStatus, { mocks })
  })
  it('show tired status', () => {
    expect(wrapper.find(TiredStatus).exists()).toBe(true)
  })
})
