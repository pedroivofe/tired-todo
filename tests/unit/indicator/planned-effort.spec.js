import { shallowMount } from '@vue/test-utils'
import PlannedEffort from '@/components/indicator/PlannedEffort.vue'
import TiringIndicator from '@/components/indicator/TiringIndicator.vue'

describe('PlannedEffort.vue', () => {
  const period = 'period'
  let effort = 12
  const maxEffort = 10
  let wrapper
  function reassignWrapper () {
    wrapper = shallowMount(PlannedEffort, {
      propsData: {
        period,
        effort,
        maxEffort
      }
    })
  }
  beforeEach(() => {
    reassignWrapper()
  })
  it("says you've planned too much for the given period", () => {
    expect(wrapper.find('span').text())
      .toMatchMessage(`You've planned .* too much for this ${period}`)
  })
  it('shows the tiring indicator when planned too much', () => {
    expect(wrapper.find(TiringIndicator).exists()).toBeTruthy()
  })
  it("doesn't show the tiring indicator when planning is ok", () => {
    effort = maxEffort - 1
    reassignWrapper()
    expect(wrapper.find(TiringIndicator).exists()).toBeFalsy()
  })
})
