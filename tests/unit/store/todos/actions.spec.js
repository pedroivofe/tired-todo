import { format } from 'date-fns'
import todoModule from '@/store/modules/todo'

const { actions } = todoModule
const index = 0
const getters = {
  getTodoIndexBydId: () => index
}
const commit = jest.fn()

describe('tickTodo', () => {
  afterAll(() => {
    commit.mockRestore()
  })
  it("marks todo as done when it isn't", () => {
    actions.tickTodo({ commit, getters }, { done: false })
    expect(commit)
      .toBeCalledWith('UPDATE_TODO', expect.objectContaining({
        index,
        todo: expect.objectContaining({ done: true })
      }))
  })
  it('marks todo as not done when it is', () => {
    actions.tickTodo({ commit, getters }, { done: true })
    expect(commit)
      .toBeCalledWith('UPDATE_TODO', expect.objectContaining({
        index,
        todo: expect.objectContaining({ done: false })
      }))
  })
  it("marks done date as today if it doesn't have a date", () => {
    actions.tickTodo({ commit, getters }, { done: false })
    const today = format(new Date(), 'yyyy-MM-dd')
    expect(commit)
      .toBeCalledWith('UPDATE_TODO', expect.objectContaining({
        index,
        todo: expect.objectContaining({
          doneDate: today,
          done: true
        })
      }))
  })
})
