import { format } from 'date-fns'
import todoModule from '@/store/modules/todo'

const { getters } = todoModule

const today = new Date()
const notToday = new Date(0) // satan
const todayFormatted = format(today, 'yyyy-MM-dd')
const notTodayFormatted = format(notToday, 'yyyy-MM-dd')
const todoToday = {
  done: false,
  tiring: 1,
  date: todayFormatted
}
const todoNotToday = {
  done: false,
  tiring: 1,
  date: notTodayFormatted
}
const doneTodoToday = {
  done: true,
  tiring: 1,
  doneDate: todayFormatted
}
const doneTodoNotToday = {
  ...doneTodoToday,
  doneDate: notToday
}

describe('tiredFromDay', () => {
  it('0 when no todos', () => {
    const tired = getters.tiredFromDay({
      todos: []
    })(today)
    expect(tired).toBe(0)
  })
  it('0 when no todos from the correct date', () => {
    const tired = getters.tiredFromDay({
      todos: [{
        done: true,
        doneDate: notTodayFormatted
      }]
    })(today)
    expect(tired).toBe(0)
  })
  it('effort of one todo from the date', () => {
    const tired = getters.tiredFromDay({
      todos: [doneTodoToday]
    })(new Date())
    expect(tired).toBe(doneTodoToday.tiring)
  })
  it('effort of multiple todos from the date', () => {
    const tired = getters.tiredFromDay({
      todos: [
        doneTodoToday,
        doneTodoToday,
        { ...doneTodoToday, tiring: 3 },
        doneTodoNotToday,
        doneTodoNotToday,
        doneTodoNotToday
      ]
    })(new Date())
    expect(tired).toBe(5)
  })
})

describe('doneThisDay', () => {
  it('is empty when no todos', () => {
    const state = {
      todos: [doneTodoNotToday, doneTodoNotToday]
    }
    expect(getters.doneThisDay(state)(today)).toHaveLength(0)
  })
  it('shows array of todos done today', () => {
    const state = {
      todos: [doneTodoNotToday, doneTodoToday, doneTodoToday]
    }
    expect(getters.doneThisDay(state)(today))
      .toEqual([doneTodoToday, doneTodoToday])
  })
})

describe('plannedThisDay', () => {
  it('is zero when no todos', () => {
    const state = {
      todos: [todoNotToday]
    }
    expect(getters.plannedThisDay(state)(today)).toEqual(0)
  })
  it('is the sum of effort for todos', () => {
    const state = {
      todos: [todoToday, todoToday, todoNotToday, todoToday]
    }
    expect(getters.plannedThisDay(state)(today))
      .toEqual(todoToday.tiring * 3)
  })
})
