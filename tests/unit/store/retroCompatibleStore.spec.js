import retroCompatibleStore from '@/store/retroCompatibleStore'

const exampleStore = { todos: [{ tiring: 5, taskName: 'desafio veus', date: '2020-04-21', id: '7a9b3de4-e2bf-456c-916a-de41739f9e39', done: true }, { tiring: 1, taskName: 'limpar cozinha', date: '2020-04-21', id: '92366bf4-8842-4b69-ac82-af4665462806', done: true }, { tiring: 1, taskName: 'whatever', date: '2020-04-22', id: 'cfcae4c0-b2a3-4894-ac89-78b9eb2231f1', done: true }, { tiring: 4, taskName: 'afinar cavaco', date: '2020-04-22', id: '8682b0ee-9ee0-46df-9735-51e0730766a6', done: true }, { tiring: 3, taskName: 'compras', date: '2020-04-22', id: 'd6bc50d4-84fd-4079-99bb-307e629e8b54', done: true }, { tiring: 4, taskName: 'melhorar app todo', date: '2020-04-21', id: 'b82fb37c-52ba-4ddd-abda-516a2b44b20e', done: true }, { tiring: 7, taskName: 'melhorar app todo', date: '2020-04-20', id: 'c99924b3-f763-4566-ac70-a6d594456c26', done: true }, { tiring: 2, taskName: 'descer o lixo', date: '2020-04-23', id: '4db2cbb5-e720-4bf2-a4fe-4fb4b7e4366d' }, { tiring: 3, taskName: 'whatever', date: '2020-04-23', id: 'c40c3394-a53d-4b3e-a2bc-d9bd509e3692' }, { tiring: 6, taskName: 'whatever', date: '2020-04-24', id: '2fa25f0e-f076-469a-9df1-4aabc6bc4c92' }, { tiring: 4, taskName: 'sadfasfd', id: 'a0b5eebc-1fb0-4966-9ec4-a27a89d30d79' }, { tiring: 6, taskName: 'asdfasdfsadf', id: '68f1b2ad-0d17-41f8-ae16-f024554cfb27' }, { tiring: 3, taskName: 'melhorar todo', date: '2020-04-23', id: '08085006-e271-4a1e-ae4c-494695f23fd3' }, { tiring: 3, taskName: 'tananã', date: '2020-04-24', id: '0b383e8c-7954-4e5d-b70c-3794e7de172a' }, { tiring: 5, taskName: 'tananã', date: '2020-04-25', id: 'f135f398-b7ec-4faa-baa5-162376c7f8aa' }, { tiring: 1, taskName: 'mexer no todo', date: '2020-04-24', id: '89240f10-b367-4b32-9cf3-803cddcc3c3b' }, { tiring: 3, taskName: 'mexer no todo', date: '2020-04-25', id: 'e360ef2d-0d92-4b57-94b5-916171b2c391' }, { tiring: 1, taskName: 'reunião', date: '2020-04-22', id: 'fc4373bb-25c3-4c0e-9127-c7bc6f4ad502', done: true }, { tiring: 4, taskName: 'asdfasdf', id: 'dcfb8660-98b5-4778-9e55-ff12d6f38a23', date: '2020-04-27' }, { tiring: 1, taskName: 'melhorar todo', date: '2020-04-22', id: 'b9de2b3f-7c58-4e70-8520-be4fb8ae64d5', done: true }, { tiring: 3, taskName: 'limpar banheiro', date: '2020-04-23', id: '03d8175e-c8e3-4ed1-8651-80ca3f941285' }], config: { maxTiredAcceptablePerWeek: 50, maxTiredAcceptable: 10 } }

const newState = retroCompatibleStore(exampleStore)

describe('retro compatible store', () => {
  it('works with a null state', () => {
    expect(retroCompatibleStore(null)).toEqual({})
  })
  it('works with a recent state', () => {
    const recentState = {
      config: {
        version: 'version'
      },
      todo: {
        todos: [
          {
            id: '20gr3hgr',
            taskName: 'task',
            done: true,
            doneDate: '2020-04-23'
          }
        ]
      }
    }
    expect(retroCompatibleStore(recentState)).toEqual(recentState)
  })
  it('has the todo module', () => {
    expect(newState.todo).toEqual(expect.objectContaining({
      todos: expect.anything()
    }))
  })
  it('has the same todos in the right place', () => {
    expect(newState.todo.todos.length).not.toBe(0)
  })
})
