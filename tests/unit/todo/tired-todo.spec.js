import { shallowMount } from '@vue/test-utils'
import { Store } from 'vuex-mock-store'
import TiredTodo from '@/components/todo/TiredTodo.vue'
import TiringIndicator from '@/components/indicator/TiringIndicator.vue'

const mockTodo = { taskName: 'task name' }
const store = new Store({
  state: {
    todo: {}
  },
  getters: {
    'todo/getTodoById': () => mockTodo
  }
})
const mocks = {
  $store: store
}

describe('TiredTodo.vue', () => {
  let wrapper
  afterEach(() => store.reset())
  beforeEach(() => {
    wrapper = shallowMount(TiredTodo, { mocks })
  })

  it('renders taskName', () => {
    const { taskName } = mockTodo
    expect(wrapper.find('span').text()).toMatch(taskName)
  })
  it('show tiring indicator', () => {
    expect(wrapper.find(TiringIndicator).exists()).toBe(true)
  })
  it('ticks todo when button clicked', () => {
    wrapper.find('button.tick-todo').trigger('click')
    expect(store.dispatch).toHaveBeenCalledWith('todo/tickTodo', mockTodo)
  })
  it('removes todo when button clicked', () => {
    wrapper.find('button.remove-todo').trigger('click')
    expect(store.dispatch).toHaveBeenCalledWith('todo/removeTodo', mockTodo)
  })
})
