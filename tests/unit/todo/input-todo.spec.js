import { shallowMount } from '@vue/test-utils'
import { Store } from 'vuex-mock-store'
import InputTodo from '@/components/todo/InputTodo.vue'
import TiredInput from '@/components/todo/TiredInput.vue'

const mockTodo = { taskName: 'task name' }
const store = new Store({
  state: {
    todo: {}
  },
  getters: {
    'todo/getTodoById': () => mockTodo
  }
})
const mocks = {
  $store: store
}
const todo = {
  tiring: 2,
  taskName: 'task'
}

describe('InputTodo.vue', () => {
  let wrapper
  afterEach(() => store.reset())
  beforeEach(() => {
    wrapper = shallowMount(InputTodo, {
      propsData: { value: todo },
      mocks
    })
  })

  it('show tired input', () => {
    expect(wrapper.find(TiredInput).exists()).toBe(true)
  })
})
