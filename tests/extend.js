function ignoreWhitespace (str) {
  return str.replace(/\r?\n|\r/g, '').replace(/\s/g, '')
}

// eslint-disable-next-line no-undef
expect.extend({
  toMatchMessage (received, expected) {
    const receivedProcessed = ignoreWhitespace(received)
    const expectedProcessed = new RegExp(ignoreWhitespace(expected))
    const pass = !!receivedProcessed.match(expectedProcessed, 'i')
    return {
      pass,
      message: () => `expected \n${received.replace(/\r?\n|\r/g, '')}\n
        to match \n${expected}\n
        ignoring case, white space and line breaks`
    }
  }
})
