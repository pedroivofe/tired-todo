import Vue from 'vue'
import VueRouter from 'vue-router'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import Home from './components/pages/Home'
import Today from './components/pages/Today'
import AllTodos from './components/pages/AllTodos'
import NoDueDate from './components/pages/NoDueDate'
import Config from './components/pages/Config'

Vue.config.productionTip = false
Vue.use(VueRouter)

const routes = [
  { path: '', component: Home },
  { path: '/today', component: Today },
  { path: '/all', component: AllTodos },
  { path: '/no-due', component: NoDueDate },
  { path: '/config', component: Config }
]

const router = new VueRouter({ routes })

new Vue({
  store,
  router,
  render: h => h(App)
}).$mount('#app')
