import {
  isSameDay,
  isSameWeek,
  isValid
} from 'date-fns'
import { zonedTimeToUtc } from 'date-fns-tz'

export const convert = str => zonedTimeToUtc(new Date(str))

export function sameDayFromString (strDay, b) {
  if (!isValid(new Date(`${strDay} 00:00:00`))) return false
  if (!isValid(new Date(b))) return false
  return isSameDay(convert(`${strDay} 00:00:00`), convert(b))
}

export function fromThisWeek (strDay) {
  return isSameWeek(convert(`${strDay} 00:00:00`), new Date())
}
