import {
  add,
  startOfWeek
} from 'date-fns'

export function daysFromWeek (date) {
  const start = startOfWeek(date)
  const thisWeek = []
  for (let i = 0; i < 7; i++) {
    thisWeek.push(add(start, { days: i }))
  }
  return thisWeek
}
