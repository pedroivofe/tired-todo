import Vue from 'vue'
import Vuex from 'vuex'
import VuexPersistence from 'vuex-persist'
import retroCompatibleStore from './retroCompatibleStore.js'
import config from './modules/config'
import todo from './modules/todo'
import toast from './modules/toast'

const vuexPersist = new VuexPersistence({
  strict: process.env.NODE_ENV !== 'production',
  restoreState (key, storage) {
    const state = JSON.parse(storage.getItem(key))
    if (state && state.toast) delete state.toast
    return retroCompatibleStore(state)
  },
  storage: window.localStorage
})

Vue.use(Vuex)

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  plugins: [vuexPersist.plugin],
  modules: {
    toast,
    todo,
    config
  }
})
