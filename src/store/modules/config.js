const state = {
  maxTiredAcceptablePerWeek: 50,
  maxTiredAcceptable: 10
}

const getters = {
}
const mutations = {
  UPDATE_MAX_TIRED_PER_WEEK (state, value) {
    state.maxTiredAcceptablePerWeek = value
  },
  UPDATE_MAX_TIRED_PER_DAY (state, value) {
    state.maxTiredAcceptable = value
  }
}

const actions = {
  updateConfig ({ commit }, {
    maxTiredAcceptable,
    maxTiredAcceptablePerWeek
  }) {
    commit('UPDATE_MAX_TIRED_PER_WEEK', maxTiredAcceptablePerWeek)
    commit('UPDATE_MAX_TIRED_PER_DAY', maxTiredAcceptable)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
