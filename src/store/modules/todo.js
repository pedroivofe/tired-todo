import Vue from 'vue'
import { v4 as uuid } from 'uuid'
import { format } from 'date-fns'
import {
  sameDayFromString,
  fromThisWeek
} from '@/services/dateSameDay'

const state = {
  todos: []
}

const getters = {
  allTodos: (state) => state.todos,
  todosNoDueDate: (state) => state.todos.filter(todo => !todo.date),
  todosFromDate: (state) => (date) => {
    return state.todos
      .filter(todo => sameDayFromString(todo.date, date))
  },
  getTodoById: (state) => (id) => {
    return state.todos.find(todo => todo.id === id)
  },
  getTodoIndexBydId: (state) => (id) => {
    return state.todos.findIndex(todo => todo.id === id)
  },
  tiredFromDay: (state) => (date) => {
    return state.todos
      .filter(todo => sameDayFromString(todo.doneDate, date))
      .filter(({ done }) => done)
      .reduce((tired, { tiring }) => tired + tiring, 0)
  },
  doneThisDay: (state) => (date) => {
    return state.todos
      .filter(todo => sameDayFromString(todo.doneDate, date))
      .filter(({ done }) => done)
  },
  tiredThisWeek (state) {
    return state.todos
      .filter(todo => todo.date && fromThisWeek(todo.date))
      .filter(({ done }) => done)
      .reduce((tired, { tiring }) => tired + tiring, 0)
  },
  plannedThisDay: (state) => (date) => {
    return state.todos
      .filter(todo => sameDayFromString(todo.date, date))
      .reduce((tired, { tiring }) => tired + tiring, 0)
  }
}

const mutations = {
  ADD_TODO (state, todo) {
    state.todos = [...state.todos, todo]
  },
  DELETE_TODO (state, id) {
    state.todos = state.todos.filter(todo => todo.id !== id)
  },
  UPDATE_TODO (state, { index, todo }) {
    const oldTodo = state.todos[index]
    Vue.set(state.todos, index, { ...oldTodo, ...todo })
  }
}

const actions = {
  tickTodo ({ commit, getters }, { id, done }) {
    const index = getters.getTodoIndexBydId(id)
    const doneDate = format(new Date(), 'yyyy-MM-dd')
    commit('UPDATE_TODO', {
      index,
      todo: {
        ...(done ? {} : { doneDate }),
        done: !done
      }
    })
  },
  updateTodo ({ commit, getters }, todo) {
    const index = getters.getTodoIndexBydId(todo.id)
    commit('UPDATE_TODO', { index, todo })
  },
  newTodo ({ commit, dispatch }, todo) {
    dispatch('toast/showMessage', 'task created', { root: true })
    commit('ADD_TODO', {
      ...todo,
      id: uuid()
    })
  },
  removeTodo ({ commit, dispatch }, todo) {
    dispatch('toast/showMessage', 'task removed', { root: true })
    commit('DELETE_TODO', todo.id)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
