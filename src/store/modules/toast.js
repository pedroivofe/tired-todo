const state = {
  show: false,
  timeout: 0,
  message: ''
}

const getters = {
}
const mutations = {
  SET_SHOW (state, value) {
    state.show = value
  },
  SET_TIMEOUT (state, value) {
    state.timeout = value
  },
  SET_MESSAGE (state, value) {
    state.message = value
  }
}

const actions = {
  showMessage ({ commit, state }, message) {
    commit('SET_MESSAGE', message)
    commit('SET_SHOW', true)
    clearTimeout(state.timeout)
    const timeout = setTimeout(() => commit('SET_SHOW', false), 5000)
    commit('SET_TIMEOUT', timeout)
  }
}

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
