# tired-todo

This is an anti-productivity todo list. Instead of having productivity goals you have effort limits to not exert yourself. Go get some rest. Reject productivity ideology. Sleep when they're working.

Currently, the "production" version is available at https://tired.graciano.me/. However, despite the retrocompatibility exercise I'm doing, it's not stable enough yet so I would say that you won't lose any data. The data is stored on your browser localStorage, and although it doesn't have an expiration date, some browser extension you have might delete it. I do plan on implementing backups and even maybe multiple device backup and cloud sync some day, but that is not on the current roadmap for this project.

## Contributing

If you are considering contributing to this project, thanks! First of all, this code is in vue 2 (planning on upgrading it) and I tried my best to give setup instructions junior developers can actually follow. If you find any problem with it, please create an issue or even an MR to improve it.

### Contributing without writing any code

I usually leave my MRs at least a couple days "marinating" if someone wants to review them or test them locally (you would need to know some git to do this). You could also try the development version available on [this link](https://main--tired-todo-list.netlify.app/#/) and try the new features before they're released, and if you find any bugs, create an issue here. Not that the development version will have a different "database" for your todos and you definetly will eventually lose data because it's a version meant for testing.

### Contributing with code

If you want to create an MR that creates new features/components, I kindly ask that you write tests for them, but it's completely okay if you don't know how to and can't write them. It's not a requirement for contributing.

### Project local development setup

You'll need to install nodejs. I recommend using [asdf](https://asdf-vm.com/) and the [node plugin](https://github.com/asdf-vm/asdf-nodejs). It gets the default version from the `.nvmrc` file in this project.

Follow [this guide](https://asdf-vm.com/guide/getting-started.html#_3-install-asdf) to install asdf. Then, add the nodejs plugin: `asdf plugin add nodejs https://github.com/asdf-vm/asdf-nodejs.git`

After that, clone this repo, cd into the created directory, and install the supported nodejs version:
```
asdf install nodejs $(cat .nvmrc)
```

Then, install the dependencies normally.
```
npm install
```

#### Compiles and hot-reloads for development
```
npm run serve
```

#### Compiles and minifies for production
```
npm run build
```

#### Run your unit tests
```
npm run test:unit
```

#### Lints and fixes files
```
npm run lint
```
## License

This project is under The Prosperity Public License 3.0.0, as can be seen on the LICENSE file, and NOT any other License as gitlab may currently automaticaly say.
